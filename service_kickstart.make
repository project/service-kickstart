; Make: http://drupalcode.org/project/drush_make.git/blob_plain/refs/heads/6.x-2.x:/README.txt

api = 2
core = 7.x

projects[drupal][version] = 7

; Required
projects[views][subdir] = contrib
projects[ctools][subdir] = contrib
projects[token][subdir] = contrib
projects[features][subdir] = contrib
projects[strongarm][subdir] = contrib
projects[date][subdir] = contrib
projects[entity][subdir] = contrib
projects[rules][subdir] = contrib

; Fields
projects[link][subdir] = contrib
projects[addressfield][subdir] = contrib

; Commerce
projects[commerce][subdir] = contrib
projects[commerce_authnet][subdir] = contrib
projects[commerce_paypal][subdir] = contrib
projects[commerce_coupon][subdir] = contrib
projects[dc_co_pages][subdir] = contrib
projects[commerce_checkout_progress][subdir] = contrib
projects[dc_cart_ajax][subdir] = contrib

; Admin & Dev
projects[devel][subdir] = contrib
projects[admin_menu][subdir] = contrib
projects[module_filter][subdir] = contrib
projects[stringoverrides][subdir] = contrib
projects[google_analytics][subdir] = contrib

; Theme
projects[rubik][type] = theme
projects[tao][type] = theme
projects[adaptivetheme][type] = theme
projects[corolla][type] = theme
projects[at-commerce][type] = theme

; Libraries
libraries[profiler][download][type] = get
libraries[profiler][download][url] = http://ftp.drupal.org/files/projects/profiler-7.x-2.0-beta1.tar.gz

