; Use this file to build a full distribution including Drupal core and the
; Service Kickstart install profile using the following command:
;
; drush make distro.make <target directory>

api = 2
core = 7.x

projects[drupal][type] = core
projects[drupal][version] = "7"

; Add Service Kickstart to the full distribution build.
projects[service_kickstart][type] = profile
projects[service_kickstart][version] = 1.x-dev
projects[service_kickstart][download][type] = git
projects[service_kickstart][download][url] = http://git.drupal.org/project/service-kickstart.git
projects[service_kickstart][download][branch] = master

